import numpy as np
import tensorflow as tf
import math
import collections
import csv
import time
import datetime as DT
from matplotlib.dates import date2num, num2date
from matplotlib import pyplot as plt
import operator
from collections import OrderedDict
import copy
import random
import argparse
from sklearn.preprocessing import MinMaxScaler
import os

earthRadius = 3961 # 3961 miles or 6373 km
########################################################
#load files
def load_data(file_name):
    csv_file_name = csv.reader(open(file_name, 'rU'), dialect='excel') 
    header_file_name = next(csv_file_name)  
    column_file_name = {}
    for h in header_file_name:
        column_file_name[h] = []
    for idx,row in enumerate(csv_file_name):
        for h, v in zip(header_file_name, row):
            column_file_name[h].append(v)
    return header_file_name, column_file_name
###############################################
#load files
def load_txt_data(file_name, header):
    print(file_name)
    column_file_name = {}; header_file_name = header
    for h in header_file_name:
        column_file_name[h] = []
    for idx, val in enumerate(open(file_name)):
        row = [_.strip('\n') for _ in val.split(',')][:-1]
        for h,v in zip(header_file_name, row):
            column_file_name[h].append(v)
    return header_file_name, column_file_name
###############################################
#load files
def convert2idx(words, offset):
    count = collections.Counter(words)
    # count_u = collections.Counter(words).most_common(vocabulary_size - 1)
    dictionary = dict()
    for word in count:
        dictionary[word] = len(dictionary)+offset
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary

def parse_args():
    parser = argparse.ArgumentParser(description="Run ebsn2vec.")
    parser.add_argument('--pretrain', nargs='?', help='0 stands for no pretrain, 1 stands for pretrain')
    parser.add_argument('--save', nargs='?', help='0 stands for not save, 1 stands for save')
    return parser.parse_args()
'************************************************'
def weighted_sample(weights, sample_size):
    """
    Returns a weighted sample without replacement. Note: weights.dtype should be float for speed, see: http://sociograph.blogspot.nl/2011/12/gotcha-with-numpys-searchsorted.html
    """
    totals = np.cumsum(weights)
    sample = []
    for i in range(sample_size):
        rnd = random.random() * totals[-1]
        idx = np.searchsorted(totals,rnd,'right')
        sample.append(idx)
        totals[idx:] -= weights[idx]
    return sample

def gen_neg_samples(e, num_sampled):
    candidate_user = copy.deepcopy(list(map_ue1.keys()))
    # g = map_eg1[e]
    for u in map_eu1[e]:
        candidate_user.remove(u)
    weights = [dictionaryU[u] for u in candidate_user]
    curd_idx = weighted_sample(weights, num_sampled)
    curd_u = [reverse_dictionaryU[weights[_]] for _ in curd_idx]
    return curd_u


def gen_batchs():
    # partition raw data into batches and stack them vertically in a data matrix
    train_es = []; train_us = []; train_attend = []
    idx_nan = 'uNan'
    for e in map_eu1:
        negative_users = gen_neg_samples(e, len(map_eu1[e])*3)
        for u in negative_users:
            train_us.append(u); train_es.append(e); train_attend.append(0)
        for u in map_eu1[e]:
            train_us.append(u); train_es.append(e); train_attend.append(1)
    'shuffle'
    rng_state = np.random.get_state()
    np.random.shuffle(train_us)
    np.random.set_state(rng_state)
    np.random.shuffle(train_es)
    np.random.set_state(rng_state)
    np.random.shuffle(train_attend)
    xs_batches = len(train_us)//batch_size
    for idx in range(xs_batches):
        idx_begin = idx*batch_size
        idx_end = (idx+1)*batch_size
        yield(train_us[idx_begin:idx_end], train_es[idx_begin:idx_end], train_attend[idx_begin:idx_end])

def gen_epochs(n):
    for i in range(n):
        yield(gen_batchs())

def _init_session():
    # adaptively growing video memory
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)

def writeout2(file_name, header_time, mydict):
    outfile = open(file_name, 'w')
    outputWriter = csv.writer(outfile)
    header = header_time
    outputWriter.writerow(header)
    print('writing ...')
    for k in mydict:
        outputWriter.writerow([k,mydict[k]])
    outfile.close()


def save_dictionary():
    header = ['key', 'value']
    writeout2(dsave_file+'user_dictionary.csv', header, dictionaryU)
    writeout2(dsave_file+'group_dictionary.csv', header, dictionaryG)

def restore_dictionary():
    header,columns = load_data(dsave_file+'user_dictionary.csv')
    dictionaryU = {k:int(v) for k,v in zip(columns['key'], columns['value'])}
    reverse_dictionaryU = dict(zip(dictionaryU.values(), dictionaryU.keys()))
    header,columns = load_data(dsave_file+'group_dictionary.csv')
    dictionaryG = {k:int(v) for k,v in zip(columns['key'], columns['value'])}
    reverse_dictionaryG = dict(zip(dictionaryG.values(), dictionaryG.keys()))
    return dictionaryU, reverse_dictionaryU, dictionaryG, reverse_dictionaryG

def dict_key_scale(my_dict):
    scaler = MinMaxScaler()
    tmp_keys = list(my_dict.keys())
    tmp_vs = scaler.fit_transform(list(my_dict.values()))
    my_dict = {k:[v] for k,v in zip(tmp_keys, tmp_vs)}
    return my_dict 

def extract_event_feature():
    header_eventgeo, column_eventgeo = load_data('Meetup_geo/event_lon_lat_crsp2rsvp.csv')
    map_e_geo = {e:[float(lon),float(lat)] for e,lon,lat in zip(column_eventgeo['eventid'], column_eventgeo['lon'], column_eventgeo['lat'])}
    tmp_cords = []
    for e in all_es:
        tmp_cords.append(map_e_geo[e])
    scaler = MinMaxScaler()
    tmp_cords1 = scaler.fit_transform(tmp_cords)
    map_e_geo = {e:cord for e, cord in zip(all_es, tmp_cords1)}

    map_estarthour = {}; map_estartweekday = {}
    for e in all_es:
        map_estarthour[e] = num2date(map_allmore_estart[e]).hour
        map_estartweekday[e] = num2date(map_allmore_estart[e]).weekday()

    map_etimeinterval = {}
    for g in map_sort_ge:
        for idx_e, e in enumerate(map_sort_ge[g]):
            if idx_e==0:
                map_etimeinterval[e] = 100
            else:
                prev_e = map_sort_ge[g][idx_e-1]
                map_etimeinterval[e] = map_allmore_estart[e] - map_allmore_estart[prev_e]
    return map_e_geo, map_estarthour, map_estartweekday, map_etimeinterval

def map_inf(columns):
    map_eg = {}; map_estart = {}; map_eu = {}; map_ue = {}; map_ge = {}
    for g,e,u,start in zip(columns['groupid'], columns['eventid'], columns['userid'], columns['event_start_time']):
        map_eg[e] = g; map_estart[e] = float(start)
        try:
            map_eu[e].append(u)
        except:
            map_eu[e] = [u]
        try:
            map_ue[u].append(e)
        except:
            map_ue[u] = [e]
        try:
            map_ge[g].append(e)
        except:
            map_ge[g] = [e]
    return map_eg, map_ge, map_estart, map_eu, map_ue

def sort_ge():
    train_g = list(map_ge1.keys())
    map_sort_ge = {}
    for g in train_g:
        tmp_et = {e:map_estart1[e] for e in map_ge1[g]} 
        try:
            for e in map_ge2[g]:
                tmp_et[e] = map_estart2[e]
        except:
            pass
        tmp_sorted = sorted(tmp_et.items(), key=operator.itemgetter(1))
        map_sort_ge[g] = list(np.array(tmp_sorted)[:,0])
    return map_sort_ge

def sort_ue():
    train_u = list(map_ue1.keys())
    map_sort_ue = {}
    for u in train_u:
        tmp_et = {e:map_estart1[e] for e in map_ue1[u]}
        try:
            for e in map_ue2[u]:
                tmp_et[e] = map_estart2[e]
        except:
            pass
        tmp_sorted = sorted(tmp_et.items(), key=operator.itemgetter(1))
        map_sort_ue[u] = list(np.array(tmp_sorted)[:,0])
    return map_sort_ue

def sort_es():
    tmp_et = {}
    for e in all_es:
        try:
            tmp_et[e] = map_estart1[e]
        except:
            tmp_et[e] = map_estart2[e]
    tmp_sorted = sorted(tmp_et.items(), key=operator.itemgetter(1))
    final_sort_es = list(np.array(tmp_sorted)[:,0])
    return final_sort_es


def extract_intersequence(u, e):
    curr_g = map_allmore_eg[e]
    first_e = map_sort_ue[u][0]; u_fist_attend_time = map_allmore_estart[first_e]
    t_e = map_allmore_estart[e]   

    curr_eidx = map_sort_ge[curr_g].index(e)
    tmp_seq = []; tmp_seqt = []; tmp_seqh = []; tmp_seqw = []; tmp_seqgeo = []
    context_attend_list = []; context_list1 = []; context_list2 = []
    if curr_eidx > num_steps:
        list1 = map_sort_ge[curr_g][(curr_eidx-num_steps):curr_eidx]
        list2 = map_sort_ge[curr_g][(curr_eidx-num_steps+1):curr_eidx+1]
    else:
        list1 = map_sort_ge[curr_g][:curr_eidx]
        list2 = map_sort_ge[curr_g][1:curr_eidx+1]
    start_ueidx = 0
    for e1,e2 in zip(list1, list2):  
        if u in map_allmore_eu[e1]:
            tmp_seq.append(1)
        elif u_fist_attend_time>map_allmore_estart[e1]:
            tmp_seq.append(2)
        else:
            tmp_seq.append(0)
        context_attend=0; context_group = dictionaryG['Nan']; context_itv = 20
        for ee in map_sort_ue[u][start_ueidx:]:
            if map_allmore_estart[ee]>=map_allmore_estart[e2]:
                break
            elif map_allmore_estart[ee]>map_allmore_estart[e1]:
                start_ueidx = start_ueidx+1
                context_attend=1
                try:
                    context_group = dictionaryG[map_allmore_eg[ee]]
                except:
                    context_group = dictionaryG['Nan']
                context_itv = int(map_allmore_estart[e2]-map_allmore_estart[ee])
        if context_itv>20:
            context_itv = 20
        context_attend_list.append(context_attend)
        context_list1.append(context_group)#group
        context_list2.append(context_itv)
        if map_etimeinterval[e2]<20:
            tmp_etv = int(map_etimeinterval[e2])
        else:
            tmp_etv = 20
        tmp_seqt.append(tmp_etv)
        tmp_seqh.append(int(map_estarthour[e2]/2))
        tmp_seqw.append(int(map_estartweekday[e2]))
        tmp_seqgeo.append(map_e_geo[e2])
    tmp_seq = [3]*(num_steps-len(tmp_seq))+ tmp_seq
    tmp_seqt = [20]*(num_steps-len(tmp_seqt))+ tmp_seqt
    tmp_seqw = [20]*(num_steps-len(tmp_seqw))+ tmp_seqw
    tmp_seqh = [20]*(num_steps-len(tmp_seqh))+ tmp_seqh
    tmp_seqgeo = [[0,0]]*(num_steps-len(tmp_seqgeo))+ tmp_seqgeo
    context_attend_list = [3]*(num_steps-len(context_attend_list)) + context_attend_list
    context_list1 = [dictionaryG['Nan']]*(num_steps-len(context_list1)) + context_list1
    context_list2 = [20]*(num_steps-len(context_list2)) + context_list2
    return(tmp_seq, tmp_seqt, tmp_seqh, tmp_seqw, tmp_seqgeo, context_list1, context_attend_list, context_list2)


def typeEvent2idxf(mix_lists):
    es_list, mye = mix_lists
    embed_gidx = []; efeatures = []
    for e in es_list:
        sub_lon = map_e_geo[e][0]
        sub_lat = map_e_geo[e][1]
        sub_hour = map_estarthour[e]
        sub_weekday = map_estartweekday[e]
        sub_time_interval = map_etimeinterval[e]
        tmp_one_row = [sub_lon, sub_lat, sub_hour, sub_weekday, sub_time_interval]
        efeatures.append(tmp_one_row)
        embed_gidx.append(dictionaryG[map_allmore_eg[e]])
    return mye, embed_gidx, efeatures

def typeUser2idxf(mix_lists):
    us_list, es_list, mye = mix_lists
    embed_uidx = []; ufeatures = []
    for u,e in zip(us_list, es_list):
        tmp_features = extract_intersequence(u, e)
        ufeatures.append(tmp_features)
        embed_uidx.append(dictionaryU[u])
    return mye, embed_uidx, ufeatures

def split_data(column_full, header, start_train_time, start_test_time):
    mystart_time = date2num(DT.datetime.strptime(start_train_time, "%Y-%m-%d %H:%M:%S"))
    myend_time = date2num(DT.datetime.strptime(start_test_time, "%Y-%m-%d %H:%M:%S"))
    column_city1 = {}; column_city2 = {}
    for h in header:
        column_city1[h] = []; column_city2[h] = []
    for idx,t in enumerate(column_full['event_start_time']):
        if float(t)>mystart_time and float(t)<myend_time:
            for h in header:
                column_city1[h].append(column_full[h][idx])
        elif float(t)>=myend_time: 
            for h in header:
                column_city2[h].append(column_full[h][idx])
    return(column_city1, column_city2)


def generate_validation_test_eu(validation_end, test_end):
    train_us = list(map_ue1.keys())
    validation_end_time = date2num(DT.datetime.strptime(validation_end, "%Y-%m-%d %H:%M:%S"))
    test_end_time = date2num(DT.datetime.strptime(test_end, "%Y-%m-%d %H:%M:%S"))
    map_validation_eu = {}; map_test_eu = {}
    for e in map_eu2:
        try:
            g = map_allmore_eg[e]
            g_in = map_ge1[g]
            et = map_allmore_estart[e]
            if float(et)<test_end_time:
                if float(et)<validation_end_time:
                    map_validation_eu[e] = list(set(map_eu2[e]).intersection(train_us))
                else:
                    map_test_eu[e] = list(set(map_eu2[e]).intersection(train_us))
        except:
            pass
    return(map_validation_eu, map_test_eu)


def transfered_node(tmp_eu):
    us = list(map_sort_ue.keys())
    EUidxs = {}; EUfs = {}; 
    Etmp_Gidxs = {}; Etmp_Efs = {}
    num_u = len(us)
    lsts = [(us, [e]*num_u, e) for e in tmp_eu]
    for e in tmp_eu:
        t1 = time.time()
        r1,r2,r3 = typeUser2idxf((us, [e]*num_u, e))
        EUidxs[r1] = r2
        EUfs[r1] = r3
        r1,r2,r3 = typeEvent2idxf(([e], e))
        Etmp_Gidxs[r1] = r2
        Etmp_Efs[r1] = r3
        # print(e, time.time()-t1)
    return EUidxs, EUfs, Etmp_Gidxs, Etmp_Efs

def run_eval(tmp_eu, EUidxs, EUfs, Etmp_Gidxs, Etmp_Efs):
    us = list(map_sort_ue.keys())
    num_u = len(us)
    TP_list = []; FN_list = []
    FP_list = []; TN_list = []
    for e in tmp_eu:
        Uidxs = EUidxs[e]; Ufs = EUfs[e]

        Useq =  [Uf[0] for Uf in Ufs]; Useqt = [Uf[1] for Uf in Ufs]
        Useqh = [Uf[2] for Uf in Ufs]; Useqw = [Uf[3] for Uf in Ufs];
        Useqgeo = [Uf[4] for Uf in Ufs]
        Ucontext = [Uf[5] for Uf in Ufs]; Ucontext_attend = [Uf[6] for Uf in Ufs]
        Ucontext_itv = [Uf[-1] for Uf in Ufs]
        tmp_Gidxs = Etmp_Gidxs[e]; tmp_Efs = Etmp_Efs[e]
        Gidxs = [tmp_Gidxs[0] for _ in range(num_u)]
        Efs = [tmp_Efs[0] for _ in range(num_u)]

        'calculate avg_loss'
        feed_dict = {input_uidx: np.array(Uidxs).reshape(-1,1), input_useq: Useq, input_useqgeo: Useqgeo, \
        input_useqt: Useqt, input_useqw: Useqw, input_useqh: Useqh, inputs_row: num_u,\
        input_ucontextg: Ucontext, input_ucontextitv: Ucontext_itv, input_ucontextattend: Ucontext_itv, \
        input_gidx: np.array(Gidxs).reshape(-1,1), input_efs: Efs, \
        keep_rate: 1.0, phase: 0, phase_RNN: 0}

        my_pred = sess.run(pred_y, feed_dict=feed_dict)
        pred_u = [us[uidx] for uidx,_ in enumerate(my_pred) if _==1]
        TP, FN, TN, FP = eval_rslt(tmp_eu[e], pred_u, num_u)
        TP_list.append(TP); FN_list.append(FN)
        TN_list.append(TN); FP_list.append(FP)
        # print('TP', TP, 'FN', FN, 'TN', TN, 'FP', FP)
    Macro_prec=sum(TP_list)/(sum(TP_list)+sum(FP_list)+0.1)
    Micro_prec=np.mean([tpp/(tpp+fpp+0.1) for tpp, fnn, fpp in zip(TP_list, FN_list, FP_list)])
    Macro_recall=sum(TP_list)/(sum(TP_list)+sum(FN_list)+0.1)
    Micro_recall=np.mean([tpp/(tpp+fnn+0.1) for tpp, fnn, fpp in zip(TP_list, FN_list, FP_list)])
    Macro_F1=2*sum(TP_list)/(2*sum(TP_list)+sum(FN_list)+sum(FP_list)+0.1)
    Micro_F1=np.mean([2*tpp/(2*tpp+fnn+fpp+0.1) for tpp, fnn, fpp in zip(TP_list, FN_list, FP_list)])
    Macro_acc=(sum(TN_list)+sum(TP_list))/(sum(TN_list)+sum(TP_list)+sum(FN_list)+sum(FP_list))
    Micro_acc=np.mean([(tnn+tpp)/(tnn+tpp+fnn+fpp) for tpp, fnn, fpp, tnn in zip(TP_list, FN_list, FP_list, TN_list)])
    print('&',round(Macro_prec,3),'&',round(Micro_prec,3),'&',round(Macro_recall,3),'&',round(Micro_recall,3),'&',round(Macro_F1,3),'&',round(Micro_F1,3),'&',round(Macro_acc,3),'&',round(Micro_acc,3),'\\')
    return ([Macro_prec, Micro_prec, Macro_recall, Micro_recall, Macro_F1, Micro_F1, Macro_acc, Micro_acc])



def eval_rslt(y_t, y_pred, num_u):
    TP = len(list(set(y_t).intersection(y_pred)))
    FN = len(y_t) - TP
    FP = len(y_pred) - TP
    TN = num_u - TP - FN - FP
    return TP, FN, TN, FP 

def embedding_feature(idx, str_name):
    with tf.variable_scope('embed4f'+str_name):
        embedding4f = tf.Variable(tf.random_uniform([8, embedding_feature_size], -1.0, 1.0), name = 'embedding4f')
        fembeds = tf.squeeze(tf.nn.embedding_lookup(embedding4f, idx), axis = 1)
        return fembeds

def embedding_input_feature(input_all, str_name):
    inputs_shape = input_all.shape[1].value
    input_ufs_ = tf.reshape(input_all, (-1,inputs_shape,1))
    hus = tf.unstack(input_ufs_, axis = 1)
    index_hus = 0; fhus = []
    for hu in hus:
        fhu = embedding_feature(hu, str(str_name)+str(index_hus))
        fhus.append(fhu)
        index_hus += 1
    rfhus = tf.concat(fhus, axis = 1)
    return rfhus

def attention(inputs, str_name, time_major=False):
    if isinstance(inputs, tuple):
        # In case of Bi-RNN, concatenate the forward and the backward RNN outputs.
        inputs = tf.concat(inputs, 2)

    if time_major:
        # (T,B,D) => (B,T,D)
        inputs = tf.transpose(inputs, [1, 0, 2])

    with tf.variable_scope('attention'+str_name):
        inputs_shape = inputs.shape
        sequence_length = inputs_shape[1].value  # the length of sequences processed in the antecedent RNN layer
        hidden_size = inputs_shape[2].value  # hidden size of the RNN layer

        # Attention mechanism
        W_omega = tf.Variable(tf.random_normal([hidden_size, attention_size], stddev=0.1))
        b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
        u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

        v = tf.tanh(tf.matmul(tf.reshape(inputs, [-1, hidden_size]), W_omega) + tf.reshape(b_omega, [1, -1]))
        vu = tf.matmul(v, tf.reshape(u_omega, [-1, 1]))
        exps = tf.reshape(tf.exp(vu), [-1, sequence_length])
        alphas = exps / tf.reshape(tf.reduce_sum(exps, 1), [-1, 1])

        output = tf.reduce_sum(inputs * tf.reshape(alphas, [-1, sequence_length, 1]), 1)

        return output

def my_RNN(cgx1, ctx2, cax3, x1, x2, x3, x4, x5, init_stateg, init_stateu, input_uidx, input_gidx, inputs_row, phase_RNN):
    with tf.device('/cpu:0'):
        'Placeholders'
        embedding4t = tf.Variable(tf.random_uniform([100, embedding_feature_size], -1.0, 1.0), name = 'embedding4t')
        embedding4h = tf.Variable(tf.random_uniform([30, embedding_feature_size], -1.0, 1.0), name = 'embedding4h')
        embedding4w = tf.Variable(tf.random_uniform([30, embedding_feature_size], -1.0, 1.0), name = 'embedding4w')
        embedding4ct = tf.Variable(tf.random_uniform([30, embedding_feature_size], -1.0, 1.0), name = 'embedding4ct')
        zero_state0 = tf.get_variable('zero_state1', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state0 = tf.matmul(tf.ones([inputs_row, 1]), zero_state0)
        Uembeddingc = tf.Variable(tf.random_uniform([Uvocabulary_size, state_size], -1.0, 1.0), dtype=tf.float32, name = 'Uembeddingc')
        init_statec = tf.squeeze(tf.nn.embedding_lookup(Uembeddingc, input_uidx),1)

        Gembeddingci = tf.Variable(tf.random_uniform([Gvocabulary_size, state_size], -1.0, 1.0), dtype=tf.float32, name = 'Gembeddingci')
        cgx1_embeds = tf.nn.embedding_lookup(Gembeddingci, cgx1)
        input_gdiff = tf.squeeze(tf.nn.embedding_lookup(Gembeddingci, input_gidx),1)

        'RNN Inputs'
        # Turn our x placeholder into a list of one-hot tensors:
        # rnn_inputs is a list of num_steps tensors with shape [batch_size, num_classes]
        cax3_one_hot = tf.one_hot(cax3, 4)
        ctx2_embeds = tf.nn.embedding_lookup(embedding4ct, ctx2)
        x1_one_hot = tf.one_hot(x1, 4) # tensor size: batch_size, num_steps, num_classes
        x2_embeds = tf.nn.embedding_lookup(embedding4t, x2)
        x3_embeds = tf.nn.embedding_lookup(embedding4h, x3)
        x4_embeds = tf.nn.embedding_lookup(embedding4w, x4)
        rnng_inputs = tf.unstack(cgx1_embeds, axis=1)
        rnnt_inputs = tf.unstack(ctx2_embeds, axis=1)
        rnnc_inputs = tf.unstack(cax3_one_hot, axis=1)
        rnn1_inputs = tf.unstack(x1_one_hot, axis=1) # unstack to num_steps of tensors: batch_size, num_classes 
        rnn2_inputs = tf.unstack(x2_embeds, axis=1)
        rnn3_inputs = tf.unstack(x3_embeds, axis=1)
        rnn4_inputs = tf.unstack(x4_embeds, axis=1)
        rnn5_inputs = tf.unstack(x5, axis=1)

        'Definition of rnn_cell'
        #This is very similar to the __call__ method on Tensorflow's BasicRNNCell. See:
        #https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/rnn/python/ops/core_rnn_cell_impl.py#L95
        #WS = tf.Variable(tf.truncated_normal([num_classes + state_size, state_size], -0.1, 0.1))
        WS1 = tf.get_variable('WS1', [3*embedding_feature_size + 2+ state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bS1 = tf.get_variable('bS1', [state_size], initializer=tf.constant_initializer(0.0))
        WI1 = tf.get_variable('WI1', [3*embedding_feature_size + 2+ state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bI1 = tf.get_variable('bI1', [state_size], initializer=tf.constant_initializer(0.0))
        WO1 = tf.get_variable('WO1', [3*embedding_feature_size + 2+ state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bO1 = tf.get_variable('bO1', [state_size], initializer=tf.constant_initializer(0.0))
        WF1 = tf.get_variable('WF1', [3*embedding_feature_size + 2+ state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bF1 = tf.get_variable('bF1', [state_size], initializer=tf.constant_initializer(0.0))

        WS2 = tf.get_variable('WS2', [4 + 3*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bS2 = tf.get_variable('bS2', [state_size], initializer=tf.constant_initializer(0.0))
        WI2 = tf.get_variable('WI2', [4 + 3*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bI2 = tf.get_variable('bI2', [state_size], initializer=tf.constant_initializer(0.0))
        WO2 = tf.get_variable('WO2', [4 + 3*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bO2 = tf.get_variable('bO2', [state_size], initializer=tf.constant_initializer(0.0))
        WF2 = tf.get_variable('WF2', [4 + 3*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bF2 = tf.get_variable('bF2', [state_size], initializer=tf.constant_initializer(0.0))

        WS3 = tf.get_variable('WS3', [2*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bS3 = tf.get_variable('bS3', [state_size], initializer=tf.constant_initializer(0.0))
        WI3 = tf.get_variable('WI3', [2*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bI3 = tf.get_variable('bI3', [state_size], initializer=tf.constant_initializer(0.0))
        WO3 = tf.get_variable('WO3', [2*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bO3 = tf.get_variable('bO3', [state_size], initializer=tf.constant_initializer(0.0))
        WF3 = tf.get_variable('WF3', [2*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bF3 = tf.get_variable('bF3', [state_size], initializer=tf.constant_initializer(0.0))

        WSc = tf.get_variable('WSc', [4+embedding_feature_size+1*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bSc = tf.get_variable('bSc', [state_size], initializer=tf.constant_initializer(0.0))
        WIc = tf.get_variable('WIc', [4+embedding_feature_size+1*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bIc = tf.get_variable('bIc', [state_size], initializer=tf.constant_initializer(0.0))
        WOc = tf.get_variable('WOc', [4+embedding_feature_size+1*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bOc = tf.get_variable('bOc', [state_size], initializer=tf.constant_initializer(0.0))
        WFc = tf.get_variable('WFc', [4+embedding_feature_size+1*state_size, state_size], initializer = tf.truncated_normal_initializer(stddev=0.1))
        bFc = tf.get_variable('bFc', [state_size], initializer=tf.constant_initializer(0.0))

        def lstm_cell1(rnn2_input, rnn3_input, rnn4_input, rnn5_input, state1c, state1h):
            input_t1 = tf.concat([rnn2_input, rnn3_input, rnn4_input, rnn5_input, state1h], 1)
            igate1 = tf.sigmoid(tf.matmul(input_t1, WI1) + bI1)
            ogate1 = tf.sigmoid(tf.matmul(input_t1, WO1) + bO1)
            fgate1 = tf.sigmoid(tf.matmul(input_t1, WF1) + bF1)
            state1c_ = tf.tanh(tf.contrib.layers.batch_norm(tf.matmul(input_t1, WS1) + bS1, scale=True, is_training=phase_RNN))
            state1c = tf.multiply(fgate1, state1c) + tf.multiply(igate1, tf.nn.dropout(state1c_, keep_rate))
            state1h = tf.multiply(igate1, tf.tanh(state1c))
            return state1c, state1h

        def lstm_cell2(rnn1_input, state1h, statech, state2c, state2h):
            input_t2 = tf.concat([rnn1_input, state1h, statech, state2h], 1)
            igate2 = tf.sigmoid(tf.matmul(input_t2, WI2) + bI2)
            ogate2 = tf.sigmoid(tf.matmul(input_t2, WO2) + bO2)
            fgate2 = tf.sigmoid(tf.matmul(input_t2, WF2) + bF2)
            state2c_ = tf.tanh(tf.contrib.layers.batch_norm(tf.matmul(input_t2, WS2) + bS2, scale=True, is_training=phase_RNN))
            state2c = tf.multiply(fgate2, state2c) + tf.multiply(igate2,tf.nn.dropout(state2c_, keep_rate))
            state2h = tf.multiply(ogate2, tf.tanh(state2c))
            return state2c, state2h

        def lstm_cell3(state2h, state3c, state3h):
            input_t3 = tf.concat([state2h, state3h], 1)
            igate3 = tf.sigmoid(tf.matmul(input_t3, WI3) + bI3)
            ogate3 = tf.sigmoid(tf.matmul(input_t3, WO3) + bO3)
            fgate3 = tf.sigmoid(tf.matmul(input_t3, WF3) + bF3)
            state3c_ = tf.tanh(tf.contrib.layers.batch_norm(tf.matmul(input_t3, WS3) + bS3, scale=True, is_training=phase_RNN))
            state3c = tf.multiply(fgate3, state3c) + tf.multiply(igate3,tf.nn.dropout(state3c_, keep_rate))
            state3h = tf.multiply(ogate3, tf.tanh(state3c))
            return state3c, state3h

        def lstm_cellc(rnnc_input, rnnt_input, rnng_input, statecc, statech):
            input_tc = tf.concat([rnnc_input, rnnt_input, statech], 1)
            igatec = tf.sigmoid(tf.matmul(input_tc, WIc) + bIc)
            ogatec = tf.sigmoid(tf.matmul(input_tc, WOc) + bOc)
            fgatec = tf.sigmoid(tf.matmul(input_tc, WFc) + bFc)
            statecc_ = tf.tanh(tf.contrib.layers.batch_norm(tf.matmul(input_tc, WSc) + bSc, scale=True, is_training=phase_RNN))
            statecc = tf.multiply(fgatec, statecc) + tf.multiply(igatec,tf.nn.dropout(statecc_, keep_rate))
            statech = tf.multiply(ogatec, tf.tanh(statecc))
            return statecc, statech


        zero_state1c = tf.get_variable('zero_state1c', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state1c = tf.matmul(tf.ones([inputs_row, 1]), zero_state1c)
        zero_state2c = tf.get_variable('zero_state2c', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state2c = tf.matmul(tf.ones([inputs_row, 1]), zero_state2c)
        zero_state3c = tf.get_variable('zero_state3c', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state3c = tf.matmul(tf.ones([inputs_row, 1]), zero_state3c)
        zero_statecc = tf.get_variable('zero_statecc', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_statecc = tf.matmul(tf.ones([inputs_row, 1]), zero_statecc)
        zero_state1h = tf.get_variable('zero_state1h', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state1h = tf.matmul(tf.ones([inputs_row, 1]), zero_state1h)
        zero_state2h = tf.get_variable('zero_state2h', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state2h = tf.matmul(tf.ones([inputs_row, 1]), zero_state2h)
        zero_state3h = tf.get_variable('zero_state3h', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state3h = tf.matmul(tf.ones([inputs_row, 1]), zero_state3h)
        zero_statech = tf.get_variable('zero_statech', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_statech = tf.matmul(tf.ones([inputs_row, 1]), zero_statech)
        state1c = init_state1c; state1h = init_state1h 
        state2c = init_state2c; state2h = init_state2h 
        state3c = init_state3c; state3h = init_state3h
        statecc = init_statecc; statech = init_statech
        rnn_outputs = []
        for rnng_input, rnnt_input, rnnc_input, rnn1_input, rnn2_input, rnn3_input, rnn4_input, rnn5_input in zip(rnng_inputs, rnnt_inputs, rnnc_inputs, rnn1_inputs, rnn2_inputs, rnn3_inputs, rnn4_inputs, rnn5_inputs):
            state1c, state1h = lstm_cell1(rnn2_input, rnn3_input, rnn4_input, rnn5_input, state1c, state1h)
            statecc, statech = lstm_cellc(rnnc_input, rnnt_input, rnng_input, statecc, statech)
            state2c, state2h = lstm_cell2(rnn1_input, state1h, statech, state2c, state2h)
            state3c, state3h = lstm_cell3(state2h, state3c, state3h)
            rnn_outputs.append(state3h)
        final_state = rnn_outputs[-1]
        # weights = tf.reduce_sum(tf.square(WS1))+tf.reduce_sum(tf.square(WS2))+tf.reduce_sum(tf.square(WS3))+\
        # tf.reduce_sum(tf.square(WI1))+tf.reduce_sum(tf.square(WI2))+tf.reduce_sum(tf.square(WI3))+\
        # tf.reduce_sum(tf.square(WO1))+tf.reduce_sum(tf.square(WO2))+tf.reduce_sum(tf.square(WO3))+\
        # tf.reduce_sum(tf.square(WF1))+tf.reduce_sum(tf.square(WF2))+tf.reduce_sum(tf.square(WF3))
        weights = tf.reduce_sum(tf.square(Uembeddingc))+ tf.reduce_sum(tf.square(Gembeddingci))
        return final_state, weights
'###############################################################'
args = parse_args()
pretrain_flag = 0# 0 stands for no pretrain, 1 stands for pretrain
get_dictionary = 0
save_flag = 1# 0 stands for no save, 1 stands for save
num_steps = 7; state_size = 32
print('pretrain_flag', pretrain_flag)
print('save_flag', save_flag)
#['CHI', 'Portland', 'NYC', 'Phoenix']
test_city = 'sample'
dsave_file = '../pretrain/esbn_'+str(test_city)+'2009/participant_ebsn2vec'
save_file = '../pretrain/esbn_'+str(test_city)+'2009/participant_ebsn2vec_us'

header_city1, column_city1 = load_data('esbn_'+str(test_city)+'2009.csv') 
header_city2, column_city2 = load_data('esbn_'+str(test_city)+'2010.csv')

column_city = {}
for h in header_city1:
    column_city[h] = column_city1[h]+column_city2[h]

column_city1, column_city2 = split_data(column_city, header_city1, "2009-01-01 00:00:01", "2010-01-01 00:00:01")
column_city = 0


'******'
if get_dictionary == 0:
    dictionaryU, reverse_dictionaryU = convert2idx(column_city1['userid'], 0)
    dictionaryU['Nan'] = len(dictionaryU); reverse_dictionaryU[dictionaryU['Nan']] = ['Nan']
    dictionaryG, reverse_dictionaryG = convert2idx(column_city1['groupid'], 0)
    dictionaryG['Nan'] = len(dictionaryG); reverse_dictionaryG[dictionaryG['Nan']] = ['Nan']
    save_dictionary()
else:
    dictionaryU, reverse_dictionaryU, dictionaryG, reverse_dictionaryG = restore_dictionary()
    print('load dictionary')
'******'

map_eg1, map_ge1, map_estart1, map_eu1, map_ue1 = map_inf(column_city1)
map_eg2, map_ge2, map_estart2, map_eu2, map_ue2 = map_inf(column_city2)
map_allmore_estart = dict(list(map_estart1.items()) + list(map_estart2.items()))
map_allmore_eg = dict(list(map_eg1.items()) + list(map_eg2.items()))
map_allmore_eu = dict(list(map_eu1.items()) + list(map_eu2.items()))
map_sort_ge = sort_ge()
map_sort_ue = sort_ue()
map_gu1 = {}
for g in map_ge1:
    map_gu1[g] = []
    for e in map_ge1[g]:
        map_gu1[g] = map_gu1[g] + map_eu1[e]
    map_gu1[g] = list(set(map_gu1[g]))
#not all event is considered, we only consider the event hosted by occured groups in training data
all_es= []
for g in map_sort_ge:
    all_es.extend(map_sort_ge[g])
sort_all_es = sort_es()

map_e_geo, map_estarthour, map_estartweekday, map_etimeinterval = extract_event_feature()
map_select_validation_eu, map_select_test_eu = generate_validation_test_eu("2010-01-14 00:00:01", "2010-03-01 00:00:01")
print('prepare datasets')
vEUidxs, vEUfs, vEtmp_Gidxs, vEtmp_Efs = transfered_node(map_select_validation_eu)
tEUidxs, tEUfs, tEtmp_Gidxs, tEtmp_Efs = transfered_node(map_select_test_eu)
# print('generated validation and test dataset')

# '###############################################################'
'parameter setting for build tf graph'
Uvocabulary_size = len(dictionaryU)+2; Gvocabulary_size = len(dictionaryG)+2
batch_size = 200; n_input = 1; num_sampled = 128
embedding_size = 256; learning_rate = 0.001; top_k = 20; num_neighbor = 8
attention_size = 32; embedding_feature_size = 8;
max_performance = [0.0]*8
print('#Users', Uvocabulary_size); print('#Groups', Gvocabulary_size);

graph = tf.Graph()
with graph.as_default():
    # Input data.
    input_uidx = tf.placeholder(tf.int32, shape=[None, 1])
    input_gidx = tf.placeholder(tf.int32, shape=[None, 1])
    input_useq = tf.placeholder(tf.int32, [None, num_steps])#class
    input_useqt = tf.placeholder(tf.int32, [None, num_steps])#
    input_useqh = tf.placeholder(tf.int32, [None, num_steps])#class
    input_useqw = tf.placeholder(tf.int32, [None, num_steps])#
    input_useqgeo = tf.placeholder(tf.float32, [None, num_steps, 2])
    input_ucontextg = tf.placeholder(tf.int32, [None, num_steps])#group
    input_ucontextitv = tf.placeholder(tf.int32, [None, num_steps])#class
    input_ucontextattend = tf.placeholder(tf.int32, [None, num_steps])#class

    inputs_row = tf.placeholder(tf.int32)
    penalty_rate = tf.placeholder(tf.float32)
    input_efs = tf.placeholder(tf.float32, shape=[None, 5])
    output_c = tf.placeholder(tf.int32, [None, ]) # 0-9 digits recognition => 10 classes
    y = tf.one_hot(output_c, 2)
    keep_rate = tf.placeholder(tf.float32)
    phase = tf.placeholder(tf.bool, name='phase')
    phase_RNN = tf.placeholder(tf.bool, name='phase_RNN')

    # Ops and variables pinned to the CPU because of missing GPU implementation
    with tf.device('/cpu:0'):
        # Look up embeddings for inputs.
        Uembeddings = tf.Variable(tf.random_uniform([Uvocabulary_size, state_size], -0.5, 0.5), dtype=tf.float32, name = 'Uembeddings')
        input_embeddingU = tf.squeeze(tf.nn.embedding_lookup(Uembeddings, input_uidx),1)
        Gembeddings = tf.Variable(tf.random_uniform([Gvocabulary_size, state_size], -0.5, 0.5), dtype=tf.float32, name = 'Gembeddings')
        input_embeddingG = tf.squeeze(tf.nn.embedding_lookup(Gembeddings, input_gidx),1)

        embedut_, weights = my_RNN(input_ucontextg, input_ucontextitv, input_ucontextattend, input_useq, input_useqt, input_useqh, input_useqw, input_useqgeo, input_embeddingG, input_embeddingU, input_uidx, input_gidx, inputs_row, phase_RNN)

        W1 = tf.Variable(tf.random_normal([state_size, state_size], stddev=0.35), name = 'W1')
        b1 = tf.Variable(tf.zeros([state_size]), name = 'b1')
        l11 = tf.contrib.layers.batch_norm(tf.add(tf.matmul(embedut_, W1), b1), scale=True, is_training=phase)
        l12 = tf.nn.relu(l11)
        l1 = tf.nn.dropout(l12, keep_rate)

        W2 = tf.Variable(tf.random_normal([state_size, state_size], stddev=0.35), name = 'W2')
        b2 = tf.Variable(tf.zeros([state_size]), name = 'b2')
        l21 = tf.contrib.layers.batch_norm(tf.add(tf.matmul(l1, W2), b2), scale=True, is_training=phase)
        l22 = tf.nn.relu(l21)
        l2 = tf.nn.dropout(l22, keep_rate)

        W3 = tf.Variable(tf.random_normal([state_size, 2], stddev=0.35), name = 'W3')
        b3 = tf.Variable(tf.zeros([2]), name = 'b3')
        out = tf.add(tf.matmul(l2, W3), b3)
        out = tf.add(tf.matmul(embedut_, W3), b3)

        pred = tf.nn.softmax(out)

    regularization_penalty = tf.reduce_sum(tf.abs(W1))+tf.reduce_sum(tf.abs(W2))+\
    tf.reduce_sum(tf.abs(W3)) + tf.reduce_sum(tf.square(Uembeddings))+\
    tf.reduce_sum(tf.square(Gembeddings))+weights

    lost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=out))
    cost = lost + tf.multiply(penalty_rate,regularization_penalty)

    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    pred_y = tf.argmax(pred, 1)
    correct_prediction = tf.equal(pred_y, tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    init = tf.global_variables_initializer()


with tf.Session(graph = graph) as sess:
    if pretrain_flag ==0:
        sess.run(init)
    else:
        tf.train.Saver().restore(sess, save_file+'.ckpt')
        print('successful load pretrained parameters')
    average_loss = 0; average_cost = 0; average_accu = 0
    for epoch_idx, epoch in enumerate(gen_epochs(20)):
        for step, (batch_us, batch_es, batch_attend) in enumerate(epoch):
            n, batch_Uidx, batch_Ufs = typeUser2idxf((batch_us, batch_es, 1))
            n, batch_Gidx, batch_Efs = typeEvent2idxf((batch_es, 1))

            # for g, attend, (tmp_seq, tmp_seqt, tmp_seqh, tmp_seqw, context, context_attend, context_itv) in zip(batch_Gidx, batch_attend, batch_Ufs):
            #     print(g, attend)
            #     print('seq',tmp_seq, 'itv',tmp_seqt, 'h',tmp_seqh, 'w', tmp_seqw)
            #     print('context', context, 'context_attend', context_attend, 'context_itv', context_itv)
            #     # for list1,list2, list_3, e_att in zip(context, context_attend,context_itv, tmp_seq):
            #     #     print(list1,list2,list_3,e_att)
            #     print('*')

            batch_seq = [Uf[0] for Uf in batch_Ufs]
            batch_seqt = [Uf[1] for Uf in batch_Ufs]
            batch_seqh = [Uf[2] for Uf in batch_Ufs]
            batch_seqw = [Uf[3] for Uf in batch_Ufs]
            batch_seqgeo = [Uf[4] for Uf in batch_Ufs]
            batch_context = [Uf[5] for Uf in batch_Ufs] 
            batch_context_attend = [Uf[6] for Uf in batch_Ufs]
            batch_context_itv = [Uf[-1] for Uf in batch_Ufs]

            feed_dict = {input_uidx: np.array(batch_Uidx).reshape(-1,1), input_useq: batch_seq, input_useqgeo: batch_seqgeo, \
            input_useqt: batch_seqt, input_useqh: batch_seqh, input_useqw: batch_seqw, inputs_row: batch_size, \
            input_ucontextg: batch_context, input_ucontextitv: batch_context_itv, input_ucontextattend: batch_context_attend, \
            input_gidx: np.array(batch_Gidx).reshape(-1,1), input_efs: batch_Efs, \
            output_c: batch_attend, keep_rate: 0.5, phase: 1, penalty_rate:0.0001, phase_RNN: 1}
            'calculate avg_loss'
            if save_flag == 1:
                _, loss_val, cost_val, accu = sess.run([train_op, lost, cost, accuracy], feed_dict=feed_dict)
            else:
                loss_val, cost_val, accu = sess.run([lost, cost, accuracy], feed_dict=feed_dict)
            average_loss += loss_val; average_cost += cost_val; average_accu += accu
            if step%200 == 0:
                average_loss /= 200; average_cost /=200; average_accu /=200
                # The average loss is an estimate of the loss over the last 2000 batches.
                print(epoch_idx, "Average loss at step ", step, ": ", average_loss, average_cost, accu)
                average_loss = 0; average_cost = 0; average_accu = 0

            if step%200 == 0:
                # print('validation:')
                # run_eval(map_select_validation_eu, vEUidxs, vEUfs, vEtmp_Gidxs, vEtmp_Efs)
                print('**')
                print('test:')
                performance = run_eval(map_select_test_eu, tEUidxs, tEUfs, tEtmp_Gidxs, tEtmp_Efs)
                if performance[-4]>max_performance[-4]:
                    max_performance = performance
                print(max_performance)

            # 'save weights'
            # if save_flag == 1 and step%5000 ==0:
            #     try:             
            #         tf.train.Saver(max_to_keep=1).save(sess, save_file+'.ckpt')
            #     except:
            #         print('need to makedir')
            #         os.makedirs(save_file, exist_ok=True)
            #         tf.train.Saver(max_to_keep=1).save(sess, save_file+'.ckpt')
            #     print("Save model to file as pretrain.")


